import React from 'react'

const FullTimeInfos = () => {
    return(
        <>
            <div className="card-panel">
                <i className='material-icons orange-txt'>filter_1</i> <p>Le convertisseur prend en compte un plafond de 26 jours ONEm par mois civil.</p>
            </div>
            <div className="card-panel">
                <i className='material-icons orange-txt'>filter_2</i> <p>Dans le cas d’une rémunération différée dans l’enseignement, vous devez multiplier le nombre de jours ONEm par 1,2.</p>
            </div>
        </>
    )
}

export default FullTimeInfos